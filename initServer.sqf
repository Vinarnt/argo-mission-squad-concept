// TODO: Fix enemies showing on map on first spawn (Need to confirm)

missionNamespace setVariable ["missionEnd", false];

// Generate spawn positions
_zones = call KF_fnc_generateZones;
zoneWest = _zones select 0;
zoneEast = _zones select 1;

_isDev = getNumber (missionConfigFile >> "isDev") == 1;

if(_isDev) then {
    [] spawn {
        sleep 1;
        _group = createGroup west;
        for[{_i = 0}, {_i < 5}, {_i = _i + 1}] do {
            _unit = _group createUnit ["B_RangeMaster_F", markerPos (zoneWest select 1), [], 0, "NONE"];
            [_unit] joinSilent _group;
            _unit setVariable ["zoneAccountable", true];
        };
        //_wp = _group addWaypoint [markerPos "move_to", 0];
        //_wp setWaypointType "MOVE";

         _group = createGroup east;
         for[{_i = 0}, {_i < 5}, {_i = _i + 1}] do {
            _unit = _group createUnit ["B_RangeMaster_F", markerPos (zoneEast select 1), [], 0, "NONE"];
            [_unit] joinSilent _group;
            _unit setVariable ["zoneAccountable", true];
         };
         //_wp = _group addWaypoint [markerPos "move_to", 0];
         //_wp setWaypointType "MOVE";
    }
};

// Don't account connecting players in zone update
addMissionEventHandler ["EntityRespawned", {
    params ["_entity", "_corpse"];

    _entity setVariable ["zoneAccountable", true];
}];

// Prevent teamkill from changing unit side
player addEventHandler ["HandleRating", {
    params ["_unit", "_rating"];

    if (_rating < 0) then {
        _unit addRating (-_rating);
    };
}];

// Ticket consuming
addMissionEventHandler ["EntityRespawned", {
    params ["_entity", "_corpse"];

    if(_entity getVariable ["firstRespawn", false]) then {
        _entity setVariable ["firstRespawn", false];
    } else {
        diag_log format ["Remove 1 ticket for %1", side group _entity];
        [side group _entity, -1] call BIS_fnc_respawnTickets;

        _westTickets = [west, 0] call BIS_fnc_respawnTickets;
        _eastTickets = [east, 0] call BIS_fnc_respawnTickets;
        if(_westTickets <= 0 || {_eastTickets <= 0}) then {
            // End mission when all tickets of a side are exhausted
            if(missionNamespace getVariable "missionEnd") exitWith {};
            missionNamespace setVariable ["missionEnd", true];
            diag_log format ["tickets exhausted"];

            if (_westTickets > 0) then {
                ["WestWon"] call BIS_fnc_endMissionServer;
             } else {
                ["EastWon"] call BIS_fnc_endMissionServer;
            };
        };
    };
}];

// Set tickets
_tickets = getNumber (missionConfigFile >> "tickets");
[west, _tickets] call BIS_fnc_respawnTickets;
[east, _tickets] call BIS_fnc_respawnTickets;

execVM "scripts\zone.sqf";
execVM "scripts\airdrops.sqf";
