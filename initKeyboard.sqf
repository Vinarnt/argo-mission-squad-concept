if (!hasInterface) exitWith {};

SL_jumpBaseHeight = 1.80;
SL_jumpMaxHeight = 3.50;
SL_jumpBaseSpeed = 0.40;
SL_jumpAnimation = "AovrPercMrunSrasWrflDf";
"SL_fn_jumpOverAnim" addPublicVariableEventHandler {
	(_this select 1) spawn SL_fn_doAnim;
};
SL_fn_doAnim = {
    params ["_unit","_velocity","_direction","_speed","_height","_anim"];
	_unit setVelocity [(_velocity select 0) + (sin _direction * _speed), (_velocity select 1) + (cos _direction * _speed), ((_velocity select 2) * _speed) + _height];
	_unit switchMove _anim;
};
earplug = 0;

keyDownHandler = {
	params ["_displayCode","_keyCode","_isShift","_isCtrl","_isAlt"];
	if ((_keyCode == 0x39) && (_isShift) && (animationState player != SL_jumpAnimation)) then {
		private ["_height","_velocity","_direction","_speed"];
		if ((player == vehicle player) && (isTouchingGround player) && ((stance player == "STAND") || (stance player == "CROUCH"))) exitWith
		{
			_height = (SL_jumpBaseHeight - (load player)) max SL_jumpMaxHeight;
			_velocity = velocity player;
			_direction = direction player;
			_speed = SL_jumpBaseSpeed;
			player setVelocity [(_velocity select 0) + (sin _direction * _speed), (_velocity select 1) + (cos _direction * _speed), ((_velocity select 2) * _speed) + _height];
			SL_fn_jumpOverAnim = [player,_velocity,_direction,_speed,_height,SL_jumpAnimation];
			publicVariable "SL_fn_jumpOverAnim";
			if (currentWeapon player == "") then // half working buggy 'fix' for having no weapon in hands (no animation available for it... BIS!!) 
			{
				player switchMove SL_jumpAnimation;
				player playMoveNow SL_jumpAnimation;
			} else {
				player switchMove SL_jumpAnimation;
			};
		};
	};
	
	shiftDown = _this select 2;
	ctrlDown = _this select 3;
	altDown = _this select 4;
	switch (_keyCode) do {
		// H
		case 0x23: {
			player action ["SWITCHWEAPON", player, player, -1];
		};

		// I
		case 0x17: {
			if(isNull findDisplay 602) then {
				player action ['Gear', player];
			};
		};

		// T
		case 0x14: {
				if (ctrlDown && shiftDown && altDown) then {
				null = [getPos cursorTarget, player] spawn {
					_group = createGroup east;
					_lightning = _group createUnit ["ModuleLightning_F", getPos player, [], 0, "CAN_COLLIDE"];
					_lightning setVariable ["hide", true];
				};
			};
		};

		// N
		case 0x31: {	//disable if player has a key set for this.
			if ((hmd player != "") && {count (actionKeys "nightvision") == 0} && {(player == vehicle player) or ctrlDown}) then {
				switch (currentVisionMode player) do {
					case 0: {player action ["nvgoggles", player];};
					case 1: {player action ["nvgogglesoff", player];};
				};
			};
		};

		// Insert
		case 0xD2: { 
			earplug = (earplug + 1) % 3;
			1 fadeSound ([1, 0.4, 0.2] select earplug);
			systemChat (["Earplugs removed", "Earplugs inserted", "Earplugs pushed deeper"] select earplug);
		};

		// F1
		case 0x3B: {
			if(not ctrlDown) then {
				hint "HELPFUL KEYS:\n
				H - Holster weapon\n
				I - Inventory\n
				N - Nightvision\n
				Shift + Space - Jump\n
				Ctrl + Shift + Alt + K - Kick yourself\n
				Insert - Insert earplugs";
			} else {
				_can_use = call BIS_fnc_isDebugConsoleAllowed;
				if (_can_use) then { createDialog "RscDisplayDebugPublic"; };
			}
		};

		// F2
		case 0x3C: {
            if (ctrlDown && ((!isServer && call BIS_fnc_admin == 2) || isServer)) then {
                if (isNull (findDisplay 404)) then { createDialog 'S3_ADMINMENU2' };
            };
		};

		// K
		case 0x25: {
		    switch true do {
		        case (ctrlDown && altDown): {
  		            player playMove "AmovPercMstpSnonWnonDnon_exerciseKata";
		        };
		        case (ctrlDown && shiftDown): {
  		            player playMove "AmovPercMstpSnonWnonDnon_exercisePushup";
		        };
		        case (ctrlDown && shiftDown && altDown): {
                    [format["%1 kicked himself out",name player]] remoteExec ["systemChat", -2];
                    [[player,"disconected"]] remoteExec ["kickplayer", 2];
		        };
		    };
		};
	};
};

keyUpHandler = {
	params ["_displayCode", "_keyCode", "_isShift", "_isCtrl", "_isAlt"];

	shiftDown = _this select 2;
	ctrlDown = _this select 3;
	altDown = _this select 4;
	switch (_keyCode) do {

	};
};

waitUntil {!isNull (findDisplay 46)};
(findDisplay 46) displaySetEventHandler ["KeyDown","_this call keyDownHandler; false;"];
(findDisplay 46) displaySetEventHandler ["KeyUp","_this call keyUpHandler; false;"];