while {true} do {
    // Update the zones
    [zoneWest, zoneEast] call KF_fnc_updateZone;
    [zoneEast, zoneWest] call KF_fnc_updateZone;

    sleep 2;
};