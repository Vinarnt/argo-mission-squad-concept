class S3_ADMINMENU2
{
	idd = 404;
	movingEnable = 1;
	class ControlsBackground
	{
		class S3_CBG
		{
			type = 0;
			idc = -1;
			x = 0.00000002;
			y = 0.10708863;
			w = 0.44834126+0.1;
			h = 0.89341773;
			style = 0;
			text = "";
			colorBackground[] = {0,0,0,0.8};
			colorText[] = {0.6157,0.0118,0.3765,1};
			font = "PuristaMedium";
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			
		};
		
	};
	class Controls
	{
		class ControlTXT:RscText
		{
			idc = -1;
			x = 0.00000002;
			y = 0.00000002;
			w = 0.4501896+0.1;
			h = 0.10392406;
			text = "MAD ADMIN TOOL";
			moving = 1;
		};
		
		class ControlLB:RscListBox
		{
			type = 5;
			idc = -1;
			x = 0.01369669;
			y = 0.12291142;
			w = 0.33279623;
			h = 0.0379747*8;
			onLoad = "uiNamespace setVariable['admin_player_list', _this select 0]; [] call AdminFunctions_fnc_updatePlayerList;";
			onLBSelChanged="playsound 'assemble_target';";
		};
		
		class S3_0:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.11556966;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Refresh";
			tooltip = "REFRESH players list";
			onButtonClick= "[] call AdminFunctions_fnc_updatePlayerList;";
			
		};
		class S3_1:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.163038;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Kick";
			tooltip = "Kick bastard";
			onMouseButtonClick = "[] call AdminFunctions_fnc_handleKickClick;";
		};
		class S3_2:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.21050637;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Mute";
			tooltip = "Mute a player";
			onButtonClick= "['VTBAN'] remoteExec ['AdminFunctions_fnc_banApply', S3_suspect, false];";
		};
		class S3_3:RscButton
		{
			idc = -1;
			x = 0.35914695;
			y = 0.25797474;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Kill";
			tooltip = "Kill player";
			onButtonClick= "[] call AdminFunctions_fnc_handleKillClick";
		};
		class S3_4:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.30544307;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "PARAMS TEST";	
			tooltip = "CHECK RECOIL/IMMORTAL/SPEED/ETC";
			onButtonClick= "['PARAMSTEST', player] remoteExec ['AdminFunctions_fnc_banApply', S3_suspect, false] ";
		};
		class S3_5:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.35291142;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Unban ALL";	
			tooltip = "";
			onButtonClick= "BAN_E setVariable['BAN_LIST', [], true];";
		};
		class S3_6:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.40037979;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Ban";
			tooltip = "Ban the player";
			onButtonClick= "[] call AdminFunctions_fnc_handleBanClick;";
		};
		
		
		class S3_7S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.44784813;
			w = 0.33279623;
			h = 0.0379747;
			text = "";
			tooltip = "Time";
			onLoad = "[_this select 0] call AdminFunctions_fnc_handleTimeSliderInit;";
		};
		
		class S3_7:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.44784813;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Set time";
			onButtonClick= "[] call AdminFunctions_fnc_handleTimeClick;";
			
		};
		class S3_8:RscButton
		{
			idc = -1;
			x = 0.35914694;
			y = 0.49531646;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Set rain";
			onButtonClick= "[] call AdminFunctions_fnc_handleRainClick;";
		};
		
		class S3_8S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.49531646;
			w = 0.33279623;
			h = 0.0379747;
			text = "";
			tooltip = "Rain";
			onLoad= "[_this select 0] call AdminFunctions_fnc_handleRainSliderInit;";
		};
		
		class S3_9:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.54797478;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Set fog";
			onButtonClick= "[] call AdminFunctions_fnc_handleFogClick;";
			
		};
		
		class S3_9S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.54797478;
			w = 0.33279623;
			h = 0.0379747;
			text = "";
			tooltip = "Fog";
			onLoad= "[_this select 0] call AdminFunctions_fnc_handleFogSliderInit;";
		};
		
		class S3_10:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.59797478;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "AI SKILL";
			onButtonClick= "['AISKILL',uiNamespace getVariable ['S3_AISKILL', [0.1]]] remoteExec ['MAD_fnc_settings', 2, false];";
		};
		
		class S3_10S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.59797478;
			w = 0.33279623;
			h = 0.0379747;
			text = "";
			tooltip = "AI skill";
			onLoad= "params['_c']; _c sliderSetRange [0, 100]; _c sliderSetPosition (S3_AI_SKILL*100); _c ctrlCommit 0;";
			onSliderPosChanged = "params['_c']; _pos= _this select 1;	_skill= _pos / 100; uiNamespace setVariable ['S3_AISKILL', [_skill]];  _c ctrlSetTooltip format['AI Skill: %1', _skill]; _c ctrlCommit 0;";
			
		};
		
		class S3_11:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.64797479;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "AREA";
			onButtonClick= "_size= uiNamespace getVariable ['MAD_AreaSize', 666];  [_size] remoteExec ['MAD_fnc_resizeArea', 0, true];";
		};
		
		class S3_11S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.64797479;
			w = 0.33279623;
			h = 0.0379747;
			text = "";
			tooltip = "Area size";
			onLoad= "params['_c']; _c sliderSetRange [1, 3000]; _c ctrlCommit 0;";
			onSliderPosChanged = "";
			
		};
		
		class S3_12:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.6979748;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "Tickets";
			onButtonClick= "";
		};
		class S3_12S:RscXSliderH
		{
			idc = -1;
			x = 0.01369669;
			y = 0.6979748;
			w = 0.33279623;
			h = 0.0379747;
			tooltip = "MAD Tickets";
			onLoad= "params['_c']; _c sliderSetRange [1, 1000]; _c ctrlCommit 0;";
			onSliderPosChanged = "";
		};
		
		class S3_13:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.74797481;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "debug";
            onMouseButtonClick = "createDialog 'RscDisplayDebugPublic';";
		};
		class S3_14:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.79797481;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "14";
           

		};
		class S3_15:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.84797482;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "15";
		};
		class S3_16:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.89797483;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "16";
		};
		class S3_17:RscButton
		{
			idc = -1;
			x = 0.35914698;
			y = 0.94797484;
			w = 0.07582939+0.1;
			h = 0.0379747;
			text = "17";
		};
	};
};