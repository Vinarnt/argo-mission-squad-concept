/*/
File: QS_icons.sqf
Script Name: Soldier Tracker
Author:

	Quiksilver (contact: armacombatgroup@gmail.com)

Version:

	2.5.0 (released 24/02/2018 A3 1.80)

Created:

	8/08/2014

Last Modified:

	26/02/2018 A3 1.80 by Quiksilver

Installation:

	In client/player init (initPlayerLocal.sqf)

		[] execVM "QS_icons.sqf";

	or

		[] execVM "scripts\QS_icons.sqf";    (if in a folder called scripts in your mission directory.)

	Follow instructions posted in the below link

		http://forums.bistudio.com/showthread.php?184108-Soldier-Tracker-(-Map-and-GPS-Icons-)
_________________________________________________________________/*/

if (isDedicated || !hasInterface) exitWith {};
private [
	'_side','_sides','_QS_ST_X','_QS_ST_map_enableUnitIcons','_QS_ST_gps_enableUnitIcons',	'_QS_ST_enableGroupIcons','_QS_ST_faction',
	'_QS_ST_iconColor_EAST','_QS_ST_iconColor_WEST',
	'_QS_ST_showMedicalWounded','_QS_ST_MedicalSystem','_QS_ST_MedicalIconColor','_QS_ST_iconShadowMap',
	'_QS_ST_iconShadowGPS','_QS_ST_iconTextSize_Map','_QS_ST_iconTextSize_GPS','_QS_ST_iconTextOffset','_QS_ST_iconSize_Man','_QS_ST_iconSize_LandVehicle',
	'_QS_ST_iconSize_Ship','_QS_ST_iconSize_Air','_QS_ST_iconSize_StaticWeapon','_QS_ST_GPSDist','_QS_ST_GPSshowNames','_QS_ST_GPSshowGroupOnly',
	'_QS_ST_autonomousVehicles','_QS_fnc_iconColor','_QS_fnc_iconType',
	'_QS_fnc_iconSize','_QS_fnc_iconPosDir','_QS_fnc_iconText','_QS_fnc_iconUnits','_QS_fnc_onMapSingleClick','_QS_fnc_mapVehicleShowCrew','_QS_fnc_iconDrawMap',
	'_QS_fnc_iconDrawGPS','_QS_ST_iconMapClickShowDetail','_QS_ST_showFriendlySides','_QS_ST_iconTextFont','_QS_ST_showAll',
	'_QS_ST_showAI','_QS_ST_showMOS','_QS_ST_showGroupOnly','_QS_ST_iconUpdatePulseDelay','_QS_ST_iconMapText','_QS_ST_showMOS_range',
	'_QS_ST_iconTextFonts','_QS_fnc_isIncapacitated','_QS_ST_htmlColorMedical','_QS_ST_R','_QS_ST_AINames',
	'_QS_ST_showOnlyVehicles','_QS_ST_iconColor_empty',
	'_QS_ST_iconSize_empty','_QS_ST_showEmptyVehicles','_QS_ST_colorInjured','_QS_ST_htmlColorInjured','_QS_ST_otherDisplays','_QS_ST_admin'
];

//==============================================================================================================================//
//=============================================================== CONFIGURATION START ==========================================//
//==============================================================================================================================//
//============================================================== FREE TO EDIT BELOW!!! =========================================//
//==============================================================================================================================//

//==================================================================================//
//================================ CONFIGURE COMMON ================================//
//==================================================================================//

//================== MASTER SWITCHES

_QS_ST_map_enableUnitIcons = true;							// BOOL. TRUE to enable MAP unit/vehicle Icons. Default TRUE.
_QS_ST_gps_enableUnitIcons = true;							// BOOL. TRUE to enable GPS unit/vehicle Icons. Default TRUE.

//================= ADMIN

_QS_ST_admin = true;										// BOOL. TRUE to enable showing all units (even enemies) if logged in as admin on a server. Default FALSE;
_QS_ST_showAll = 0;											// NUMBER. Intended for Debug / Development use only! Caution: Will cause lag if 1 or 2! Settings -  0 = Disabled (Recommended). 1 = Reveal all Units + vehicles. 2 = Reveal all mission objects + vehicles + units. May override below configurations if set at 1 or 2.

//================= DEFAULT ICON COLORS by FACTION

_QS_ST_iconColor_EAST = [0.5,0,0,0.65];							// ARRAY (NUMBER). RGBA color code.	Default [0.5,0,0,0.65];
_QS_ST_iconColor_WEST = [0,0.4,1,0.65];						// ARRAY (NUMBER). RGBA color code. Default [0,0.3,0.6,0.65];

//================= MEDICAL

_QS_ST_showMedicalWounded = true;								// BOOL. TRUE to show wounded on the map and GPS. FALSE to not show wounded on the map with this script. Default TRUE.
_QS_ST_MedicalSystem = [										// ARRAY(STRING). The Active Medical System. Uncomment ONLY ONE. FIRST UNCOMMENTED ONE WILL BE USED. Comment the rest out as shown. Do not add commas and only allow 1 to be uncommented.
	'BIS'														// BIS Revive.
	//'BTC'														// BTC Revive.
	//'AIS'														// AIS Revive.
	//'ACE'														// ACE 3 Revive.
	//'FAR'														// Farooq's Revive.
	//'AWS'    													// A3 Wounding System by Psycho.
];
_QS_ST_MedicalIconColor = [1,1,1,0.9];							// ARRAY (NUMBER). Color of medical icons in RGBA format. Default [1,0.41,0,1];
_QS_ST_colorInjured = [0.75,0.55,0,0.75];						// ARRAY (NUMBER). RGBA color code. Color of units with > 10% damage, in map group interactive interface. Default [0.7,0.6,0,0.5];

//==================================================================================//
//=========================== CONFIGURE MAP (UNIT/VEHICLE) ICONS ===================//
//==================================================================================//

_QS_ST_showAI = true;											// BOOL. FALSE = players only, TRUE = players and AI. Default TRUE.
_QS_ST_AINames = false;											// BOOL. Set TRUE to show human names for AI with the map/vehicle icons. Set FALSE and will be named 'AI'. Default FALSE.
_QS_ST_iconMapText = true;										// BOOL. TRUE to show unit/vehicle icon text on the map. FALSE to only show the icon and NO text (name/class). Default TRUE.
_QS_ST_showMOS = false;											// BOOL. TRUE = show Military Occupational Specialty text(unit/vehicle class/role display name), FALSE = disable and only show icons and names. Default FALSE.
_QS_ST_showMOS_range = 300;									// NUMBER. Range in distance to show MOS on the map. Default 3500.
_QS_ST_showGroupOnly = false;									// BOOL. Set TRUE to show ONLY the unit icons of THE PLAYERS GROUP MEMBERS on the MAP, FALSE to show ALL your factions units. May override other config. Default TRUE.
_QS_ST_showOnlyVehicles = false;								// BOOL. Set TRUE to show ONLY vehicles, no foot-soldier units will be shown. May override other config. Default TRUE.
_QS_ST_iconMapClickShowDetail = true;							// BOOL. Set TRUE to show unit/vehicle detail when player clicks on their map near the vehicle. Only works for shown vehicles. Default TRUE.
_QS_ST_iconUpdatePulseDelay = 0.25;								// NUMBER. How often should location of unit on the MAP be updated? 0 = as fast as possible, else if > 0 then it = time in seconds. Default 0.
_QS_ST_iconShadowMap = 1;										// NUMBER. Icon Shadow on MAP. 0 = no shadow. 1 = shadow. 2 = outline. Must be 0, 1, or 2. Default 1.
_QS_ST_iconTextSize_Map = 0.04;									// NUMBER. Icon Text Size on MAP display. Default is 0.05.
_QS_ST_iconTextOffset = 'right';								// STRING. Icon Text Offset. Can be 'left' or 'center' or 'right'. Default is 'right'
_QS_ST_iconSize_Man = 16;										// NUMBER. Icon Size by Vehicle Type. Man/Units. Default = 22
_QS_ST_iconSize_LandVehicle = 22;								// NUMBER. Icon Size by Vehicle Type. Ground-based vehicles. Default = 26
_QS_ST_iconSize_Ship = 20;										// NUMBER. Icon Size by Vehicle Type. Water-based vehicles. Default = 24
_QS_ST_iconSize_Air = 20;										// NUMBER. Icon Size by Vehicle Type. Air vehicles. Default = 24
_QS_ST_iconSize_StaticWeapon = 19;								// NUMBER. Icon Size by Vehicle Type. Static Weapon (Mortar, remote designator, HMG/GMG. Default = 22
_QS_ST_iconTextFonts = [										// ARRAY (STRING). Icon Text Font. Only the uncommented one will be used. Do not add commas and only allow 1 to be uncommented. Default 'puristaMedium'.
	//'EtelkaMonospacePro'
	//'EtelkaMonospaceProBold'
	//'EtelkaNarrowMediumPro'
	//'LucidaConsoleB'
	//'PuristaBold'
	//'PuristaLight'
	//'puristaMedium'
	//'PuristaSemibold'
	'TahomaB'
];
_QS_ST_otherDisplays = true;									// BOOL. TRUE to add Unit/Vehicle Icon support for UAV Terminal and Artillery Computer. Runs a separate script to handle these displays. Only works if  _QS_ST_map_enableUnitIcons = TRUE;

//==================================================================================//
//=========================== CONFIGURE GPS (UNIT/VEHICLE) ICONS ===================//
//==================================================================================//

_QS_ST_GPSDist = 150;											// NUMBER. Distance from player that units shown on GPS. Higher number = lower script performance. Not significant but every 1/10th of a frame counts! Default 300
_QS_ST_GPSshowNames = false;									// BOOL. TRUE to show unit names on the GPS display. Default FALSE.
_QS_ST_GPSshowGroupOnly = false;								// BOOL. TRUE to show only group members on the GPS display. Default TRUE.
_QS_ST_iconTextSize_GPS = 0.04;									// NUMBER. Icon Text Size on GPS display. Default is 0.05.
_QS_ST_iconShadowGPS = 2;										// NUMBER. Icon Shadow on GPS. 0 = no shadow. 1 = shadow. 2 = outline. Must be 0, 1, or 2. Default 1.

//==================================================================================//
//============================= CONFIGURE BONUS FEATURES ===========================//
//==================================================================================//

_QS_ST_showEmptyVehicles = false;								// BOOL. TRUE to mark certain unoccupied vehicles on the map. The vehicle must be assigned this variable:    <vehicle> setVariable ['QS_ST_drawEmptyVehicle',TRUE,TRUE];    Default FALSE.   Only works if  _QS_ST_map_enableUnitIcons = TRUE;
_QS_ST_iconColor_empty = [0.7,0.6,0,0.5];						// ARRAY (NUMBERS). Color of unoccupied vehicles, in RGBA. Default = [0.7,0.6,0,0.5];
_QS_ST_iconSize_empty = 20;										// NUMBER. Icon size of unoccupied vehicles, if shown.

//==============================================================================================================================//
//=============================================================== CONFIGURATION END ============================================//
//==============================================================================================================================//
//===================================================== EDITING BELOW FOR ADVANCED USERS ONLY!!! ===============================//
//==============================================================================================================================//

_QS_fnc_isIncapacitated = {
	params ['_u','_med'];
	if ((lifeState _u) isEqualTo 'INCAPACITATED') exitWith {true};
	private _r = false;
	if (_med isEqualTo 'BTC') then {
		if (!isNil {_u getVariable 'BTC_need_revive'}) then {
			if ((_u getVariable 'BTC_need_revive') isEqualTo 1) then {
				_r = true;
			};
		};
	} else {
		if (_med isEqualTo 'FAR') then {
			if (!isNil {_u getVariable 'FAR_isUnconscious'}) then {
				if ((_u getVariable 'FAR_isUnconscious') isEqualTo 1) then {
					_r = true;
				};
			};
		} else {
			if (_med isEqualTo 'AIS') then {
				if (!isNil {_u getVariable 'unit_is_unconscious'}) then {
					if ((_u getVariable 'unit_is_unconscious')) then {
						_r = true;
					};
				};
			} else {
				if (_med isEqualTo 'AWS') then {
					if (!isNil {_u getVariable 'tcb_ais_agony'}) then {
						if ((_u getVariable 'tcb_ais_agony')) then {
							_r = true;
						};
					};
				} else {
					if (_med isEqualTo 'ACE') then {
						if (!isNil {_u getVariable 'ACE_isUnconscious'}) then {
							if ((_u getVariable 'ACE_isUnconscious')) then {
								_r = true;
							};
						};
					};
				};
			};
		};
	};
	_r;
};
_QS_fnc_iconColor = {
	params [['_v',objNull],['_ds',1],'_QS_sideT_X',['_ms',1]];
	_u = effectiveCommander _v;
	_side = side (group _u);
	private _exit = false;
	private _color = _QS_ST_X select 13;
	private _a = 0;
	if (!(_v isKindOf 'Man')) then {
		if (_v getVariable ['QS_ST_drawEmptyVehicle',false]) then {
			if ((count (crew _v)) isEqualTo 0) then {
				_exit = true;
				_color = _QS_ST_X select 78;
				_color set [3,0.65];
			};
		};
	};
	if (_exit) exitWith {_color;};
	private _useTeamColor = false;
	if ((group _u) isEqualTo (group player)) then {
		_useTeamColor = true;
		_a = 0.85;
	} else {
		_a = 0.65;
	};
	if (_QS_ST_X select 14 && {[_u,((_QS_ST_X select 15) select 0)] call (_QS_ST_X select 69)}) then { // Show wounded
        _exit = true;
        _color = _QS_ST_X select 16;
        if(side group _u == side group player) then { // Check if unit is in team
            _color set [3,_a];
        } else {
            _color set [3, 0];
        };
	} else {
		if ([_u,((_QS_ST_X select 15) select 0)] call (_QS_ST_X select 69)) then { // Incapacited
			_exit = true;
			_color = _QS_ST_X select 16;
			_color set [3,0]; // Hide
		};
	};
	if (_exit) exitWith {_color;};
	if (_useTeamColor) then {
		if (isNull (objectParent _u)) then {
			private _teamID = 0;
			if (!isNil {assignedTeam _u}) then {
				_teamID = ['MAIN','RED','GREEN','BLUE','YELLOW'] find (assignedTeam _u);
				if (_teamID isEqualTo -1) then {
					_teamID = 0;
				};
			};
			if (_side isEqualTo east) then {_color = _QS_ST_X select 9;};
			if (_side isEqualTo west) then {_color = _QS_ST_X select 10;};
			_color = [_color,[1,0,0,1],[0,1,0.5,1],[0,0.5,1,1],[1,1,0,1]] select _teamID;
			_color set [3,_a];
			_exit = true;
		};
	};
    if (_exit) exitWith {_color;};
    if (_side isEqualTo east) exitWith {
        _color = _QS_ST_X select 9;
        _color set [3,_a];
        _color;
    };
    if (_side isEqualTo west) exitWith {
        _color = _QS_ST_X select 10;
        _color set [3,_a];
        _color;
    };
};
_QS_fnc_iconType = {
	params ['_u'];
	private _vt = typeOf (vehicle _u);
	private _i = missionNamespace getVariable [format ['QS_ST_iconType#%1',_vt],''];
	if (_i isEqualTo '') then {
		if ((vehicle _u) isKindOf 'CAManBase') then {
			if (_u getUnitTrait 'medic') then {
				_vt = 'B_medic_F';
			} else {
				if (_u getUnitTrait 'engineer') then {
					_vt = 'B_engineer_F';
				} else {
					if (_u getUnitTrait 'explosiveSpecialist') then {
						_vt = 'B_soldier_exp_F';
					};
				};
			};
		};
		_i = getText (configFile >> 'CfgVehicles' >> _vt >> 'icon');
		missionNamespace setVariable [format ['QS_ST_iconType#%1',_vt],_i];
	};
	_i;
};
_QS_fnc_iconSize = {
	params ['_v','','_QS_ST_X'];
	private _i = missionNamespace getVariable [(format ['QS_ST_iconSize#%1',(typeOf _v)]),0];
	if (_i isEqualTo 0) then {
		if (_v isKindOf 'Man') then {_i = _QS_ST_X select 22;_i;};
		if (_v isKindOf 'LandVehicle') then {_i = _QS_ST_X select 23;_i;};
		if (_v isKindOf 'Air') then {_i = _QS_ST_X select 25;_i;};
		if (_v isKindOf 'StaticWeapon') then {_i = _QS_ST_X select 26; _i;};
		if (_v isKindOf 'Ship') then {_i = _QS_ST_X select 24;_i;};
		missionNamespace setVariable [(format ['QS_ST_iconSize#%1',(typeOf _v)]),_i,false];
	};
	_i;
};
_QS_fnc_iconPosDir = {
	params ['_v','_ds','_dl'];
	private _posDir = [[0,0,0],0];
	if (_ds isEqualTo 1) then {
		if (_dl > 0) then {
			if (diag_tickTime > (missionNamespace getVariable 'QS_ST_iconUpdatePulseTimer')) then {
				_posDir = [getPosASLVisual _v,getDirVisual _v];
				_v setVariable ['QS_ST_lastPulsePos',_posDir,false];
			} else {
				if (!isNil {_v getVariable 'QS_ST_lastPulsePos'}) then {
					_posDir = _v getVariable 'QS_ST_lastPulsePos';
				} else {
					_posDir = [getPosASLVisual _v,getDirVisual _v];
					_v setVariable ['QS_ST_lastPulsePos',_posDir,false];
				};
			};
		} else {
			_posDir = [getPosASLVisual _v,getDirVisual _v];
		};
	} else {
		_posDir = [getPosASLVisual _v,getDirVisual _v];
	};
	_posDir;
};
_QS_fnc_iconText = {
	params ['_v','_ds','_QS_ST_X',['_ms',1]];

	_showMOS = _QS_ST_X select 64;
	_showAINames = _QS_ST_X select 71;
	private _t = '';
	private _n = 0;
	private _vt = missionNamespace getVariable [format ['QS_ST_iconVehicleDN#%1',(typeOf _v)],''];
	if (_vt isEqualTo '') then {
		_vt = getText (configFile >> 'CfgVehicles' >> (typeOf _v) >> 'displayName');
		missionNamespace setVariable [format ['QS_ST_iconVehicleDN#%1',(typeOf _v)],_vt];
	};
	if (!(_QS_ST_X select 64)) then {
		_vt = '';
	};
	private _vn = name ((crew _v) select 0);
	if (!isPlayer ((crew _v) select 0)) then {
		if (!(_showAINames)) then {
			_vn = '[AI]';
		};
	};
	_isAdmin = (((call (missionNamespace getVariable 'BIS_fnc_admin')) isEqualTo 2) && (_QS_ST_X select 86));
	if (((_v distance2D player) < (_QS_ST_X select 68)) || {(_isAdmin)}) then {

	    if (_showMOS) then {
		    _t = format ['%1 [%2]',_vn,_vt];
        } else {
		    _t = format ['%1',_vn];
		};
	} else {
		_t = format ['%1',_vn];
	};
	if ((_v isKindOf 'LandVehicle') || {(_v isKindOf 'Air')} || {(_v isKindOf 'Ship')}) then {
		_n = 0;
		_n = (count (crew _v)) - 1;
		if (_n > 0) then {
			if (!isNil {_v getVariable 'QS_ST_mapClickShowCrew'}) then {
				if (_v getVariable 'QS_ST_mapClickShowCrew') then {
					_t = '';
					private _crewIndex = 0;
					private _na = '';
					_crewCount = (count (crew _v)) - 1;
					{
						_na = name _x;
						if (!(_showAINames)) then {
							if (!isPlayer _x) then {
								_na = '[AI]';
							};
						};
						if (!(['error',_na,false] call (missionNamespace getVariable 'BIS_fnc_inString'))) then {
							if (!(_crewIndex isEqualTo _crewCount)) then {
								_t = _t + _na + ', ';
							} else {
								_t = _t + _na;
							};
						};
						_crewIndex = _crewIndex + 1;
					} count (crew _v);
				} else {
					if (!isNull driver _v) then {
						if (_showMOS) then {
                            _t = format ['%1 [%2] +%3',_vn,_vt,_n];
                        } else {
                            _t = format ['%1 +%2',_vn,_n];
                        };
					} else {
						if (_showMOS) then {
                            _t = format ['[%1] %2 +%3',_vt,_vn,_n];
                        } else {
                            _t = format ['%1 +%2',_vn,_n];
                        };
					};
				};
			} else {
				if (!isNull driver _v) then {
                    if (_showMOS) then {
					    _t = format ['%1 [%2] +%3',_vn,_vt,_n];
					} else {
						_t = format ['%1 +%2',_vn,_n];
					};
				} else {
				    if (_showMOS) then {
					    _t = format ['[%1] %2 +%3',_vt,_vn,_n];
					} else {
						_t = format ['%1 +%2',_vn,_n];
					};
				};
			};
		} else {
			if (!isNull driver _v) then {
			    if (_showMOS) then {
				    _t = format ['%1 [%2]',_vn,_vt];
				} else {
					_t = format ['%1',_vn];
				};
			} else {
			    if (_showMOS) then {
				    _t = format ['[%1] %2',_vt,_vn];
            	} else {
                	_t = format ['%1',_vn];
				}
			};
		};
	};
	_t;
};
_QS_fnc_iconUnits = {
	params ['_di','_QS_ST_X'];
	private _exit = FALSE;
	private _si = [EAST,WEST];
	private _as = [];
	private _au = [];
	_isAdmin = (((call (missionNamespace getVariable 'BIS_fnc_admin')) isEqualTo 2) && (_QS_ST_X select 86));
	if ((_QS_ST_X select 61) > 0) exitWith {
		if ((_QS_ST_X select 61) isEqualTo 1) then { // Show all units and vehicles
			_au = allUnits + vehicles;
		};
		if ((_QS_ST_X select 61) isEqualTo 2) then { // Show all entities
			_au = entities [[],[],TRUE,TRUE];
		};
		_au;
	};
	if (((_di isEqualTo 1) && ((_QS_ST_X select 65))) && {(!(_QS_ST_X select 75))}) then { // Show player group only
		_exit = TRUE;
		_au = units (group player);
		if ((_QS_ST_X select 80)) then {
			{
				if (!(_x in _au)) then {
					if (_x getVariable ['QS_ST_drawEmptyVehicle',FALSE]) then {
						if ((crew _x) isEqualTo []) then {
							0 = _au pushBack _x;
						};
					};
				};
			} count vehicles;
		};
		_au;
	};
	if ((_di isEqualTo 2) && ((_QS_ST_X select 29))) then {
		_exit = TRUE;
		_au = units (group player);
		_au;
	};
	if (_exit) exitWith {_au;};
    if (isMultiplayer) then {
        if (_isAdmin) then {
            {
                0 = _as pushBack _x;
            } count _si;
        } else {
            _as pushBack (_si select (_QS_ST_X select 3));
        };
    };
	if (!(_QS_ST_X select 63)) then {
		if (isMultiplayer) then {
			if (_isAdmin) then {
				{
					if (_x isEqualTo ((crew (vehicle _x)) select 0)) then {
						0 = _au pushBack _x;
					};
				} count allUnits;
			} else {
				{
					if (((side group _x) in _as) || {(captive _x)}) then {
						if (isPlayer _x) then {
							if (_di isEqualTo 2) then {
								if ((_x distance2D player) <= (call (_QS_ST_X select 87))) then {
									if (_x isEqualTo ((crew (vehicle _x)) select 0)) then {
										0 = _au pushBack _x;
									};
								};
							} else {
								if (_x isEqualTo ((crew (vehicle _x)) select 0)) then {
									0 = _au pushBack _x;
								};
							};
						};
					};
				} count (allPlayers + allUnitsUav);
			};
		};
	} else {
		{
			if (((side group _x) in _as) || {(captive _x)}) then {
				if (_di isEqualTo 2) then {
					if ((_x distance2D player) < (call (_QS_ST_X select 87))) then {
						if (_x isEqualTo ((crew (vehicle _x)) select 0)) then {
							0 = _au pushBack _x;
						};
					};
				} else {
					if (_x isEqualTo ((crew (vehicle _x)) select 0)) then {
						0 = _au pushBack _x;
					};
				};
			};
		} count allUnits;
	};
	if ((_di isEqualTo 1) && (_QS_ST_X select 75)) exitWith {
		_auv = [];
		{
			if (!((vehicle _x) isKindOf 'Man')) then {
				0 = _auv pushBack _x;
			};
		} count _au;
		if ((_QS_ST_X select 80)) then {
			{
				if (!(_x in _auv)) then {
					if (_x getVariable ['QS_ST_drawEmptyVehicle',FALSE]) then {
						if ((crew _x) isEqualTo []) then {
							0 = _auv pushBack _x;
						};
					};
				};
			} count vehicles;
		};
		if ((_QS_ST_X select 65)) then {
			{
				0 = _auv pushBack _x;
			} count (units (group player));
		};
		_auv;
	};
	if ((_di isEqualTo 1) && (_QS_ST_X select 80)) exitWith {
		{
			if (!(_x in _au)) then {
				if (_x getVariable ['QS_ST_drawEmptyVehicle',FALSE]) then {
					if ((crew _x) isEqualTo []) then {
						0 = _au pushBack _x;
					};
				};
			};
		} count vehicles;
		_au;
	};
	_au;
};
_QS_fnc_onMapSingleClick = {
	params ['_units','_position','_alt','_shift'];
	if ((!(_alt)) && (!(_shift))) then {
		if (player getVariable 'QS_ST_mapSingleClick') then {
			player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
			if (alive (player getVariable ['QS_ST_map_vehicleShowCrew',objNull])) then {
				(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
			};
		};
		comment "player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];";
		player setVariable ['QS_ST_mapSingleClick',TRUE,FALSE];
		private _vehicle = objNull;
		_vehicles = (nearestObjects [_position,['Air','LandVehicle','Ship'],250,TRUE]) select {(alive _x)};
		if ((count _vehicles) > 0) then {
			if ((count _vehicles) > 1) then {
				private _dist = 999999;
				{
					if ((_x distance2D _position) < _dist) then {
						_dist = _x distance2D _position;
						_vehicle = _x;
					};
				} forEach _vehicles;
			} else {
				_vehicle = _vehicles select 0;
			};
		};
		_QS_ST_X = [] call (missionNamespace getVariable 'QS_ST_X');
		if (alive _vehicle) then {
			if ((count (crew _vehicle)) > 1) then {
				if ((side group (effectiveCommander _vehicle)) isEqualTo playerSide) then {
					if (!(_vehicle isEqualTo (player getVariable ['QS_ST_map_vehicleShowCrew',objNull]))) then {
						player setVariable ['QS_ST_map_vehicleShowCrew',_vehicle,FALSE];
						_vehicle setVariable ['QS_ST_mapClickShowCrew',TRUE,FALSE];
					} else {
						(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
						player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
						player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
					};
				} else {
					(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
					player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
					player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
				};
			} else {
				(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
				player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
				player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
			};
		} else {
			(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
			player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
			player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
		};
	};
	if (_shift) then {
		if (player isEqualTo (leader (group player))) then {
			private _nearUnit = objNull;
			_nearUnits = (nearestObjects [_position,['CAManBase'],250,TRUE]) select {((alive _x) && ((group _x) isEqualTo (group player)) && (isNull (objectParent _x)))};
			if ((count _nearUnits) > 0) then {
				if ((count _nearUnits) > 1) then {
					private _dist = 999999;
					{
						if ((_x distance2D _position) < _dist) then {
							_dist = _x distance2D _position;
							_nearUnit = _x;
						};
					} forEach _nearUnits;
				} else {
					_nearUnit = _nearUnits select 0;
				};
			};
			if (alive _nearUnit) then {
				player groupSelectUnit [_nearUnit,(!(_nearUnit in _units))];
			};
		};
	};
};
_QS_fnc_GPSDistance = {
    private _GPSDistance = _QS_ST_X select 27;
    private _scaledGPSDistance = 1500 min (_GPSDistance max (_GPSDistance * ((speed player / 100) * 5)));

    // return
    _scaledGPSDistance;
};
_QS_fnc_mapVehicleShowCrew = {};
_QS_fnc_iconDrawMap = {
	_m = _this select 0;
	_QS_ST_X = [] call (missionNamespace getVariable 'QS_ST_X');
	if (diag_tickTime > (missionNamespace getVariable 'QS_ST_updateDraw_map')) then {
		missionNamespace setVariable ['QS_ST_updateDraw_map',(diag_tickTime + 3),FALSE];
		missionNamespace setVariable ['QS_ST_drawArray_map',([1,_QS_ST_X] call (_QS_ST_X select 46)),FALSE];
	};
	_sh = _QS_ST_X select 17;
	_ts = _QS_ST_X select 19;
	_tf = _QS_ST_X select 60;
	_to = _QS_ST_X select 21;
	_de = _QS_ST_X select 66;
	_player = player;
	_ms = ctrlMapScale _m;
	private _ve = objNull;
	private _po = [[0,0,0],0];
	private _is = 0;
	if (!((missionNamespace getVariable 'QS_ST_drawArray_map') isEqualTo [])) then {
		{
			if (!isNull _x) then {
				_ve = vehicle _x;
				if (alive _ve) then {
					_po = [_ve,1,_de] call (_QS_ST_X select 44);
					_is = [_ve,1,_QS_ST_X] call (_QS_ST_X select 43);
					if (_ve isEqualTo (vehicle _player)) then {
						_m drawIcon ['a3\ui_f\data\igui\cfg\islandmap\iconplayer_ca.paa',[1,0,0,0.75],(_po select 0),24,24,(_po select 1),'',0,0.03,_tf,_to];
					};
					_m drawIcon [
						([_ve,1,_QS_ST_X] call (_QS_ST_X select 42)),
						([_ve,1,_QS_ST_X,_ms] call (_QS_ST_X select 41)),
						(_po select 0),
						_is,
						_is,
						(_po select 1),
						([_ve,1,_QS_ST_X,_ms] call (_QS_ST_X select 45)),
						_sh,
						_ts,
						_tf,
						_to
					];
				};
			};
		} count (missionNamespace getVariable ['QS_ST_drawArray_map',[]]);
	};
	if (_de > 0) then {
		if (diag_tickTime > (missionNamespace getVariable 'QS_ST_iconUpdatePulseTimer')) then {
			missionNamespace setVariable ['QS_ST_iconUpdatePulseTimer',(diag_tickTime + _de)];
		};
	};
};
_QS_fnc_iconDrawGPS = {
	_m = _this select 0;
	_QS_ST_X = [] call (missionNamespace getVariable 'QS_ST_X');
	if (diag_tickTime > (missionNamespace getVariable 'QS_ST_updateDraw_gps')) then {
		missionNamespace setVariable ['QS_ST_updateDraw_gps',(diag_tickTime + 3),FALSE];
		missionNamespace setVariable ['QS_ST_drawArray_gps',([2,_QS_ST_X] call (_QS_ST_X select 46)),FALSE];
	};
	if (!((missionNamespace getVariable 'QS_ST_drawArray_gps') isEqualTo [])) then {
		_sh = _QS_ST_X select 18;
		_ts = _QS_ST_X select 20;
		_tf = _QS_ST_X select 60;
		_to = _QS_ST_X select 21;
		_de = _QS_ST_X select 66;
		private _ve = objNull;
		private _po = [[0,0,0],0];
		private _is = 0;
		private _distance = call (_QS_ST_X select 87);
		{
			if (!isNull _x && {_x != player}) then {
				_ve = vehicle _x;
				if (alive _ve) then {
					_po = [_ve,2,_de] call (_QS_ST_X select 44);
					_is = [_ve,2,_QS_ST_X] call (_QS_ST_X select 43);
					_m drawIcon [
						([_ve,2,_QS_ST_X] call (_QS_ST_X select 42)),
						([_ve,2,_QS_ST_X] call (_QS_ST_X select 41)),
						(_po select 0),
						_is,
						_is,
						(_po select 1),
						([_ve,2,_QS_ST_X] call (_QS_ST_X select 45)),
						_sh,
						_ts,
						_tf,
						_to
					];

                    if(player distance2D _ve <= _distance) then {
					    _m drawLine [
					        (position player),
					        (position _ve),
					        ([_ve,2,_QS_ST_X] call (_QS_ST_X select 41))
					    ];
                    };
				};
			};
		} count (missionNamespace getVariable ['QS_ST_drawArray_gps',[]]);
	};
};

scriptName 'Soldier Tracker by Quiksilver - (Init)';
_side = playerSide;
_sides = [EAST,WEST];
uiSleep 0.1;
_QS_ST_faction = _sides find _side;
_QS_ST_autonomousVehicles = [];
if (!(_QS_ST_iconShadowMap in [0,1,2])) then {
	_QS_ST_iconShadowMap = 1;
};
if (!(_QS_ST_iconShadowGPS in [0,1,2])) then {
	_QS_ST_iconShadowGPS = 1;
};
if (_QS_ST_iconUpdatePulseDelay > 0) then {
	missionNamespace setVariable ['QS_ST_iconUpdatePulseTimer',diag_tickTime];
};
_QS_ST_iconTextFont = _QS_ST_iconTextFonts select 0;
_QS_ST_htmlColorMedical = [_QS_ST_MedicalIconColor select 0,_QS_ST_MedicalIconColor select 1,_QS_ST_MedicalIconColor select 2,_QS_ST_MedicalIconColor select 3] call (missionNamespace getVariable 'BIS_fnc_colorRGBtoHTML');
_QS_ST_htmlColorInjured = [_QS_ST_colorInjured select 0,_QS_ST_colorInjured select 1,_QS_ST_colorInjured select 2,_QS_ST_colorInjured select 3] call (missionNamespace getVariable 'BIS_fnc_colorRGBtoHTML');

_QS_ST_R = [
	_QS_ST_map_enableUnitIcons,     // 0
	_QS_ST_gps_enableUnitIcons,     // 1
	-1,                             // 2
	_QS_ST_faction,                 // 3
	-1,                             // 4
	-1,                             // 5
	-1,                             // 6
	-1,                             // 7
	-1,                             // 8
	_QS_ST_iconColor_EAST,          // 9
	_QS_ST_iconColor_WEST,          // 10
	-1,                             // 11
	-1,                             // 12
	[0, 0, 0, 0],                   // 13
	_QS_ST_showMedicalWounded,      // 14
	_QS_ST_MedicalSystem,           // 15
	_QS_ST_MedicalIconColor,        // 16
	_QS_ST_iconShadowMap,           // 17
	_QS_ST_iconShadowGPS,           // 18
	_QS_ST_iconTextSize_Map,        // 19
	_QS_ST_iconTextSize_GPS,        // 20
	_QS_ST_iconTextOffset,          // 21
	_QS_ST_iconSize_Man,            // 22
	_QS_ST_iconSize_LandVehicle,    // 23
	_QS_ST_iconSize_Ship,           // 24
	_QS_ST_iconSize_Air,            // 25
	_QS_ST_iconSize_StaticWeapon,   // 26
	_QS_ST_GPSDist,                 // 27
	_QS_ST_GPSshowNames,            // 28
	_QS_ST_GPSshowGroupOnly,        // 29
	-1,                             // 30
	-1,                             // 31
	-1,                             // 32
	-1,                             // 33
	-1,                             // 34
	-1,                             // 35
	-1,                             // 36
	-1,                             // 37
	-1,                             // 38
	-1,                             // 39
	_QS_ST_autonomousVehicles,      // 40
	_QS_fnc_iconColor,              // 41
	_QS_fnc_iconType,               // 42
	_QS_fnc_iconSize,               // 43
	_QS_fnc_iconPosDir,             // 44
	_QS_fnc_iconText,               // 45
	_QS_fnc_iconUnits,              // 46
	_QS_fnc_onMapSingleClick,       // 47
	_QS_fnc_mapVehicleShowCrew,     // 48
	_QS_fnc_iconDrawMap,            // 49
	_QS_fnc_iconDrawGPS,            // 50
	-1,                             // 51
	-1,                             // 52
	-1,                             // 53
	-1,                             // 54
	-1,                             // 55
	_QS_ST_iconMapClickShowDetail,  // 56
	-1,                             // 57
	-1,                             // 58
	-1,                             // 59
	_QS_ST_iconTextFont,            // 60
	_QS_ST_showAll,                 // 61
	-1,                             // 62
	_QS_ST_showAI,                  // 63
	_QS_ST_showMOS,                 // 64
	_QS_ST_showGroupOnly,           // 65
	_QS_ST_iconUpdatePulseDelay,    // 66
	_QS_ST_iconMapText,             // 67
	_QS_ST_showMOS_range,           // 68
	_QS_fnc_isIncapacitated,        // 69
	_QS_ST_htmlColorMedical,        // 70
	_QS_ST_AINames,                 // 71
	-1,                             // 72
	-1,                             // 73
	-1,                             // 74
	_QS_ST_showOnlyVehicles,        // 75
	-1,                             // 76
	-1,                             // 77
	_QS_ST_iconColor_empty,         // 78
	_QS_ST_iconSize_empty,          // 79
	_QS_ST_showEmptyVehicles,       // 80
	_QS_ST_htmlColorInjured,        // 81
	_QS_ST_otherDisplays,           // 82
	-1,		                        // 83
	-1,		                        // 84
	-1,		                        // 85
	_QS_ST_admin,					// 86
	_QS_fnc_GPSDistance             // 87
];
{
	missionNamespace setVariable _x;
} forEach [
	['QS_ST_X',(compileFinal (str _QS_ST_R)),FALSE],
	['QS_ST_updateDraw_map',(diag_tickTime + 2),FALSE],
	['QS_ST_updateDraw_gps',(diag_tickTime + 1),FALSE],
	['QS_ST_drawArray_map',[],FALSE],
	['QS_ST_drawArray_gps',[],FALSE]
];
waitUntil {
	uiSleep 0.1;
	!(isNull (findDisplay 12))
};
_QS_ST_X = [] call (missionNamespace getVariable 'QS_ST_X');
if (_QS_ST_X select 0) then {
	((findDisplay 12) displayCtrl 51) ctrlAddEventHandler ['Draw',(format ['_this call %1',(_QS_ST_X select 49)])];
	if (_QS_ST_X select 82) then {
		[_QS_ST_X] spawn {
			scriptName 'Soldier Tracker by Quiksilver - Splendid Camera, Artillery Computer and UAV Terminal support';
			private ['_QS_display1Opened','_QS_display2Opened','_QS_display3Opened'];
			_QS_ST_X = _this select 0;
			_QS_display1Opened = FALSE;
			_QS_display2Opened = FALSE;
			_QS_display3Opened = FALSE;
			disableSerialization;
			for '_x' from 0 to 1 step 0 do {
				if (!(_QS_display1Opened)) then {
					if (!isNull ((findDisplay 160) displayCtrl 51)) then {
						_QS_display1Opened = TRUE;
						((findDisplay 160) displayCtrl 51) ctrlAddEventHandler ['Draw',(format ['_this call %1',(_QS_ST_X select 49)])];
					};
				} else {
					if (isNull ((findDisplay 160) displayCtrl 51)) then {
						_QS_display1Opened = FALSE;
					};
				};
				if (!(_QS_display2Opened)) then {
					if (!isNull((findDisplay -1) displayCtrl 500)) then {
						_QS_display2Opened = TRUE;
						((findDisplay -1) displayCtrl 500) ctrlAddEventHandler ['Draw',(format ['_this call %1',(_QS_ST_X select 49)])];
					};
				} else {
					if (isNull ((findDisplay -1) displayCtrl 500)) then {
						_QS_display2Opened = FALSE;
					};
				};
			    // Splendid Camera Map
			    private _splendidMapIDD = 314;
			    private _splendidMapControlIDC = 3141;
				if (!(_QS_display3Opened)) then {
					if (!isNull ((findDisplay _splendidMapIDD) displayCtrl _splendidMapControlIDC)) then {
						_QS_display3Opened = TRUE;
						((findDisplay _splendidMapIDD) displayCtrl _splendidMapControlIDC) ctrlAddEventHandler ['Draw',(format ['_this call %1',(_QS_ST_X select 49)])];
					};
				} else {
					if (isNull ((findDisplay _splendidMapIDD) displayCtrl _splendidMapControlIDC)) then {
						_QS_display3Opened = FALSE;
					};
				};
				uiSleep 0.25;
			};
		};
	};
	if (_QS_ST_X select 56) then {
		player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
		player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
		{
			addMissionEventHandler _x;
		} forEach [
			['MapSingleClick',(_QS_ST_X select 47)],
			[
				'Map',
				{
					params ['_mapIsOpened','_mapIsForced'];
					if (!(_mapIsOpened)) then {
						if (alive (player getVariable ['QS_ST_map_vehicleShowCrew',objNull])) then {
							player setVariable ['QS_ST_mapSingleClick',FALSE,FALSE];
							(player getVariable ['QS_ST_map_vehicleShowCrew',objNull]) setVariable ['QS_ST_mapClickShowCrew',FALSE,FALSE];
							player setVariable ['QS_ST_map_vehicleShowCrew',objNull,FALSE];
						};
					};
				}
			]
		];
	};
};

if (_QS_ST_X select 1) then {
	[_QS_ST_X] spawn {
		scriptName 'Soldier Tracker (GPS Icons) by Quiksilver - Waiting for GPS display';
		params ['_QS_ST_X'];
		disableSerialization;
		private _gps = controlNull;
		private _exit = FALSE;
		for '_x' from 0 to 1 step 0 do {
			{
				if (['133',(str _x),FALSE] call BIS_fnc_inString) then {
					if (!isNull (_x displayCtrl 101)) exitWith {
						_gps = (_x displayCtrl 101);
						_gps ctrlRemoveAllEventHandlers 'Draw';
						_gps ctrlAddEventHandler ['Draw',(format ['_this call %1',(_QS_ST_X select 50)])];
						_exit = TRUE;
					};
				};
			} forEach (uiNamespace getVariable 'IGUI_displays');
			uiSleep 0.25;
			if (_exit) exitWith {};
		};
	};
};