#define layer_1_name "intro_1"
#define layer_2_name "intro_2"

_text = getText (missionConfigFile >> "introText") +
        "<br /><br />Developped by Kyu";

_camera = "camera" camCreate [4400,6079,2];
_camera cameraEffect ["internal", "back"];
_camera camSetTarget intro_table;
_camera camCommit 0;
showCinemaBorder true;

uiNamespace setVariable ["introCamera", _camera];

_layer1 = layer_1_name call BIS_fnc_rscLayer;
_layer2 = layer_2_name call BIS_fnc_rscLayer;
[_text, 0, 0.2, 9999999, 1, 0, _layer1] spawn BIS_fnc_dynamicText;

uiSleep 2;
_handlerID = findDisplay 46 displayAddEventHandler ["KeyDown", {
    params ["_display", "_key"];

    if(_key == 0x39) then {
        _display displayRemoveEventHandler ["KeyDown", uiNamespace getVariable "introSpaceHandler"];

        (layer_1_name call BIS_fnc_rscLayer) cutText ["", "PLAIN"];
        (layer_2_name call BIS_fnc_rscLayer) cutText ["", "PLAIN"];

        _camera = uiNamespace getVariable "introCamera";
        _camera cameraEffect ["terminate", "back"];
        camDestroy _camera;
        showCinemaBorder false;

        uiNamespace setVariable ["introCamera", nil];
        uiNamespace setVariable ["introSpaceHandler", nil];

        forceRespawn player;

        // Show instructions
        "Instructions" hintC [
            "Welcome to this squad concept mission.\nIt's still a work in progress and is subject to change.",
            "The mission is based on cooperation.",
            "Both sides (Clouds and Flames) are squads.",
            "Each squad have a zone.",
            "Those zone positions are updated in realtime based on the position of the squad players.",
            "So if you want to move the zone in a direction, all players have to move in that direction.",
            "If you are out of the squad zone, you have 10 seconds to come back in before being killed.",
            "When a player is killed, his squad will lost a ticket.",
            "The mission end when a squad exhausted their tickets.\n",
            "Make your loadouts in the armory, 'Equipment' on the game main menu.\n",
            "Have fun and don't forget to give feedbacks and suggestions"
        ];
    };
}];
uiNamespace setVariable ["introSpaceHandler", _handlerID];

["Press <t color='#FFBF00'>SPACE</t> to continue", 0, 0.7, 9999999, 1, 0, _layer2] spawn BIS_fnc_dynamicText;