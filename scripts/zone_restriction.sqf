#define CHECK_RATE 0.5;
#define RETURN_DELAY 10;

private ["_isIn", "_fnc_onActivation", "_fnc_onDeactivation", "_handle"];

_isIn = false;
_handle = scriptNull;

_fnc_onActivation = {
    params ["_handle"];

    if(!isNull _handle) then {
        terminate _handle;
    };
    "alert" cutText ["", "PLAIN"];
};

_fnc_onDeactivation = {
    _handle = [] spawn {
        playSound "AlarmCar";
        for [{_i = RETURN_DELAY}, {_i >= 0 }, {_i = _i - 1}] do {
            "alert" cutText [format ["<t size='2.5'>Return with your squad, you'll be killed in <t color='#ff0000'>%1</t> second(s)</t>", _i], "PLAIN", -1, true, true];
            sleep 1;
        };
        "alert" cutText ["", "PLAIN"];
        forceRespawn player;
    };

    // return
    _handle;
};

while {true} do {
    if((lifeState player) in ["HEALTHY", "INJURED"]) then {
        if(side group player == west) then {
            if(player inArea "westZoneMarker") then {
                // Entered
                if(!_isIn) then {
                    _isIn = true;
                    [_handle] call _fnc_onActivation;
                }
            } else { // Exited
                if(_isIn) then {
                    _isIn = false;
                    _handle = call _fnc_onDeactivation;
                };
            };
        } else {
            if (side group player == east) then {
                if(player inArea "eastZoneMarker") then {
                    // Entered
                    if(!_isIn) then {
                        _isIn = true;
                        [_handle] call _fnc_onActivation;
                    }
                } else { // Exited
                    if(_isIn) then {
                        _isIn = false;
                        _handle = call _fnc_onDeactivation;
                    };
                };
            };
        };
    };
    sleep CHECK_RATE;
};