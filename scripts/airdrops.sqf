private ["_duration", "_delay"];

_duration = getNumber (missionConfigFile >> "airdropDuration");
_delay = getNumber (missionConfigFile >> "airdropDelay");
_minRadius = 5;
_maxRadius = (getNumber (missionConfigFile >> "zoneRadius")) * 0.5;

while {true} do {
    sleep _delay;
    [_duration, _minRadius, _maxRadius] spawn {
        params ["_duration", "_minRadius", "_maxRadius"];

        [west, markerPos (zoneWest select 0), _minRadius, _maxRadius, _duration] call KF_fnc_airdrop;
    };
    [_duration, _minRadius, _maxRadius] spawn {
        params ["_duration", "_minRadius", "_maxRadius"];

        [east, markerPos (zoneEast select 0), _minRadius, _maxRadius, _duration] call KF_fnc_airdrop;
    };
};
