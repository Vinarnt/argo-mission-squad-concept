BIS_invReqOK = sendCloudRequestClient ["users/current/loadouts/simple?source=respawn", "_this call BIS_fnc_ARGOApplyLoadouts"];
BIS_fnc_ARGOApplyLoadouts = {
    if (count _this == 1 || getLoginStatus != 1) then {
        _this = [];
    } else {
        _this = _this select 2;
    };

    BIS_fnc_saveInventory_data = [];
    _loadoutNames = [];
    _tracerSuffix = if (side group player == WEST) then {"_green"} else {"_red"};
    _loadoutsApplied = [];
    {
        if (count _x > 0) then {
            _loadoutsApplied pushBack (_forEachIndex + 1);
            _loadoutArr = _x;
            _loadoutData = [];
            _loadoutName = _loadoutArr select 0;
            _loadoutNames pushBack _loadoutName;
            _duplicities = count (_loadoutNames select {_x == _loadoutName});
            if (_duplicities > 1) then {
                _loadoutName = format ["%1 #%2", _loadoutName, _duplicities];
            };
            BIS_fnc_saveInventory_data pushBack _loadoutName;
            _grenades = _loadoutArr select 6;
            _grenadesArr = switch (_grenades) do {
                case "GrenadesSmokeRGO": {["HandGrenade", "SmokeShell"]};
                case "GrenadesSmokeSmoke": {["SmokeShell", "SmokeShell"]};
                case "GrenadesRGORGO": {["HandGrenade", "HandGrenade"]};
                default {[]};
            };
            _magsArr = [];
            _wpnPrimary = _loadoutArr select 1;
            _wpnHandgun = _loadoutArr select 5;
            if (_wpnPrimary != "") then {
                _weaponTypeArr = toArray _wpnPrimary;
                _weaponTypeArr resize 3;
                _weaponType = toLower toString _weaponTypeArr;
                _compatibleMags = getArray (configFile >> "CfgWeapons" >> _wpnPrimary >> "magazines");
                _primaryMagType = _compatibleMags select 0;
                _tracerMagType = _primaryMagType + _tracerSuffix;
                _magType = if (_tracerMagType in _compatibleMags) then {_tracerMagType} else {_primaryMagType};
                _magsCnt = switch (_weaponType) do {
                    case "ari";
                    case "sri": {5};
                    case "lmg";
                    case "mmg": {3};
                    case "hgu";
                    case "smg": {10};
                    default {5};
                };
                if (getNumber (configFile >> "CfgMagazines" >> _magType >> "count") > 50) then {
                    _magsCnt = 3;
                };
                for [{_j = 1}, {_j <= _magsCnt}, {_j = _j + 1}] do {
                    _magsArr pushBack _magType;
                };
                _muzzles = getArray (configFile >> "CfgWeapons" >> _wpnPrimary >> "muzzles");
                if (count _muzzles > 1) then {
                    _glMuzzle = _muzzles select 1;
                    _glType = (getArray (configFile >> "CfgWeapons" >> _wpnPrimary >> _glMuzzle >> "magazines")) select 0;
                    _magsArr append [_glType, _glType];
                };
            };
            if (_wpnHandgun != "") then {
                _magTypeHGun = (getArray (configFile >> "CfgWeapons" >> _wpnHandgun >> "magazines")) select 0;
                for [{_k = 1}, {_k <= 3}, {_k = _k + 1}] do {
                    _magsArr pushBack _magTypeHGun;
                };
            };
            _loadoutData pushBack [if (side group player == WEST) then {"U_Clouds_AM"} else {"U_Flames_AM"}, _magsArr + _grenadesArr];
            _vest = _loadoutArr select 7;
            if (_vest != "") then {
                if (side group player == WEST) then {
                    _vest = _vest + "_C";
                };
            };
            _loadoutData pushBack [_vest, []];
            _loadoutData pushBack [(_loadoutArr select 11), []];
            _loadoutData pushBack (_loadoutArr select 9);
            _loadoutData pushBack (_loadoutArr select 10);
            _loadoutData pushBack "";
            _loadoutData pushBack [_wpnPrimary, [_loadoutArr select 3, "", _loadoutArr select 2, _loadoutArr select 4], _magsArr select 0];
            _loadoutData pushBack ["", ["", "", "", ""], ""];
            _loadoutData pushBack [_wpnHandgun, ["", "", "", ""], _magsArr select ((count _magsArr) - 1)];
            _loadoutData pushBack ["ItemMap","ItemCompass","ItemRadio", "ItemGPS"];
            BIS_fnc_saveInventory_data pushBack _loadoutData;
            [player, format ["missionNamespace:%1", _loadoutName]] call BIS_fnc_addRespawnInventory;
        };
    } forEach _this;

    _defaultLoadouts = [
        "AG_AKS_AM",
        "AG_Mk20_AM",
        "AG_TRG_AM",
        "AG_Protector_AM",
        "AG_Vermin_AM"
    ];
    _missingLoadouts = [1,2,3,4,5] - _loadoutsApplied;
    _loadoutSuffix = if (side group player == WEST) then {"_C"} else {"_F"};

    {
        _loadout = _defaultLoadouts select (_x - 1);
        _loadout = _loadout + _loadoutSuffix;
        [player, _loadout] call BIS_fnc_addRespawnInventory;
    } forEach _missingLoadouts;

    // Load default loadouts if request failed
    if (!BIS_invReqOK) then {
        _defaultLoadouts = [
            "AG_AKS_AM",
            "AG_Mk20_AM",
            "AG_TRG_AM",
            "AG_Protector_AM",
            "AG_Vermin_AM"
        ];
        _loadoutSuffix = if (side group player == WEST) then {"_C"} else {"_F"};
        {
            _loadout = _x + _loadoutSuffix;
            [player, _loadout] call BIS_fnc_addRespawnInventory;
        } forEach _defaultLoadouts;
        [] spawn {
            sleep 3;
            [localize "STR_argo_fnc_initcommon_5", "", TRUE] spawn BIS_fnc_GUImessage;
        };
    };
};