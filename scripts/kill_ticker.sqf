init = {
    addMissionEventHandler ["EntityKilled", {
        params ["_killed", "_killer", "_instigator"];

        if(!(_killed isKindOf "Man")) exitWith {};

        if (isNull _instigator) then {_instigator = vehicle _killer};
        if (isNull _instigator) then {_instigator = _killer};
        //diag_log format ["killed: %1, killer: %2, instigator: %3", _killed, _killed, _instigator];
        _newKill = [_killed, _killer, _instigator];
        if (count tlq_killArray > 100) then {tlq_killArray = []};

        tlq_killArray set [count tlq_killArray, _newKill call tlq_parseKill];

        [] spawn tlq_killList;
        if (player == _killer) then {_newKill spawn tlq_killPopUp};
    }];
};

tlq_parseKill = {
    params ["_victim", "_killer", "_instigator"];

	_line = "";
	_killerName = "";
	_victimName = "";
	_killerString = "";
	_killerColor = "#99D5FF";
	_victimColor = "#99D5FF";
	_shotDistance = round (_victim distance _killer);

	if (!(isPlayer _killer)) then {
		_killerName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _killer] >> "Displayname");
		if(vehicle _killer != _killer) then {
			_killerName = format["%1 %2",getText (configFile >> "CfgVehicles" >> format["%1",typeOf vehicle _killer] >> "Displayname"), (assignedVehicleRole _killer) select 0]
		};
	} else { _killerName = name _killer };

	if (!(isPlayer _victim)) then {
		_victimName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _victim] >> "Displayname");
		if(vehicle _victim != _victim) then {
			_victimName = format["%1 %2",getText (configFile >> "CfgVehicles" >> format["%1",typeOf (vehicle _victim)] >> "Displayname"), (assignedVehicleRole _victim) select 0]
		};
	} else { _victimName = name _victim };

	if ((_killer==player) or (_killer == vehicle player)) then {
		_killerColor = "#ffff00"; //yellow
	}
	else
	{
		_killerColor = side group _killer call BIS_fnc_sideColor;
		_r = _killerColor select 0;
		_g = _killerColor select 1;
		_b = _killerColor select 2;
		_killerColor = [_r+0.1,_g+0.1,_b+0.1];
		_killerColor = _killerColor call BIS_fnc_colorRGBtoHTML;
	};

	if (_victim == player) then {
		_victimColor = "#ffff00"; //yellow
	}
	else
	{
		_victimColor = side group _victim call BIS_fnc_sideColor;
		_r = _victimColor select 0;
		_g = _victimColor select 1;
		_b = _victimColor select 2;
		_victimColor = [_r + 0.1, _g + 0.1, _b + 0.1];
		_victimColor = _victimColor call BIS_fnc_colorRGBtoHTML;
	};

	_killerString = format["<t color='%1'>%2</t>", _killerColor , _killerName];
	_victimString = format["<t color='%1'>%2</t>", _victimColor, _victimName];

	//the line which shows the final formatted kill
	_line = switch(true) do {
		case(_killer == _victim): { format ["%1 killed himself", _killerString] };
		case(isNull _killer): { format ["Bad luck for %1", _victimString] };
		case (!(_instigator isKindOf "Man")): { format  ["%1 killed %2 (Road kill)", _killerString, _victimString] };
		default {
		    _weaponIcon = getText (configFile >> "CfgWeapons" >> (currentWeapon _killer) >> "iconLog");
		    if(_weaponIcon == "") then {
		        _weaponType = getText (configFile >> "CfgWeapons" >> (currentWeapon _killer) >> "cursor");
                if(_weaponType == "rocket") then {
                    _weaponType = "AT"
                };
                _weaponIcon = getText (configFile >> "CfgWeaponIcons" >> _weaponType);
		    };
		    format ["%1 <img size='0.9' image='%2'/> %3 (%4m)", _killerString, _weaponIcon, _victimString, _shotDistance]
		};
	};
	_line;
};

tlq_killPopUp = {
    params ["_victim", "_killer"];

	_victimName = "";
	_victimString = "";
	_victimColor = "#99D5FF";

	if (!(isPlayer _victim)) then {
		_victimName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _victim] >> "Displayname");
        if(vehicle _victim != _victim) then {
            _victimName = getText (configFile >> "CfgVehicles" >> format["%1 crew",typeOf vehicle _victim] >> "Displayname");
        };
	} else{
	    _victimName = name _victim;
	};

	_victimColor = (side group _victim call BIS_fnc_sideColor) call BIS_fnc_colorRGBtoHTML;

	_victimString = format["<t color='%1'>%2</t>",_victimColor,_victimName];

	_line = if ((_killer == player) and (_victim == player)) then {
		"<t size='0.5'>You killed yourself</t>";
	} else {
		format ["<t size='0.5'>You killed %1</t>",_victimString];
	};	

 	[_line,0,0.875,2,0,0,7017] spawn bis_fnc_dynamicText;

};

tlq_killList = {
	//flush kills and  show most recent
	if (time - tlq_killTime > 37) then {
		tlq_displayedKills = [];
	};

	tlq_displayedKills set [count tlq_displayedKills, tlq_killArray select (count tlq_killArray - 1)];

	_tickerText = "";
	_c = 0;
	for "_i" from (count tlq_displayedKills) to 0 step -1 do {
		_c = _c + 1;
		_tickerText = format ["%1<br />%2",tlq_displayedKills select _i,_tickerText];
		if (_c > 8) exitWith{};
	};

	hintSilent parseText _tickerText;
	tlq_killTime = time;
};

//declare global variables
tlq_killArray = [];
tlq_displayedKills = [];
tlq_killTime = 0;

[] call init;

