/*
	author: Kyu
	description: Drop a rearming box.
	returns: Nothing
*/

params ["_side", "_position", "_radiusMin", "_radiusMax", "_duration"];
private ["_para", "_item", "_smoke01", "_smoke02", "_airdropNumber"];

_airdropNumber = missionNamespace getVariable ["airdrop_number", 0];
_airdropNumber = _airdropNumber + 1;
missionNamespace setVariable ["airdrop_number", _airdropNumber];

["Airdrop"] remoteExec ["BIS_fnc_showNotification", _side];

_airdropPosition = [_position, _radiusMin, _radiusMax] call BIS_fnc_ARGOFindAirdropLandingPos;
_para = createVehicle ["B_Parachute_02_F", _airdropPosition, [], 0, "FLY"];
_para setPos [_airdropPosition select 0, _airdropPosition select 1, 150];
playSound3D ["ARGO\Scenarios_ARGO\Sounds\flyby.wss", _para, false];
_item = createVehicle ["IG_supplyCrate_F", [10,10,10], [], 0, "NONE"];
_item allowDamage false;
clearWeaponCargoGlobal _item;
clearMagazineCargoGlobal _item;
clearItemCargoGlobal _item;
clearBackpackCargoGlobal _item;

_item attachTo [_para, [0, 0, -0.4]];
_smoke01 = "#particlesource" createVehicle [10,10,10];
_smoke01 setParticleClass "SmokeFlareGreen";
_smoke01 attachTo [_item,[0,0,0]];
_smoke02 = "#particlesource" createVehicle [10,10,10];
_smoke02 setParticleClass "SmokeFlareGreen2";
_smoke02 attachTo [_item,[0,0,0]];

_null = _para spawn {
    _lastH = (position _this) select 2;
    sleep 1;
    while {(position _this) select 2 >= 2} do {
        if (abs (_lastH - ((position _this) select 2)) < 3) exitWith {_this setVariable ["BIS_stuck", true]};
        _lastH = (position _this) select 2;
        sleep 1;
    }
};
while {position _para select 2 >= 2 && !isNull _item && !(_para getVariable ["BIS_stuck", false])} do {
    _para setVelocity [0, 0, (velocity _para) select 2];
    _para setVectorUp [0,0,1];
    sleep 0.1;
};
if (_para getVariable ["BIS_stuck", false]) then {
    detach _item;
    deleteVehicle _para;
};
if (isNull _item) then {
    deleteVehicle _smoke01;
    deleteVehicle _smoke02;
    deleteVehicle _para;
} else {
    detach _item;
    _para disableCollisionWith _item;
    [_smoke01, _smoke02, _item, format ["airdrop_%1", _airdropNumber], _side, _duration] spawn {
        params ["_smoke01", "_smoke02", "_item", "_markerName", "_side", "_duration"];

        if !(isNull attachedTo _smoke01) then {
            _JIPId = [[_markerName, _item, _duration], {
                params ["_markerName", "_item", "_duration"];

                [
                    _item,
                    "Rearm",
                    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_revive_ca.paa",
                    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_revive_ca.paa",
                    "_this distance _target < 5",
                    "_caller distance _target < 5",
                    {},
                    {},
                    {
                        [player, [missionNamespace, "BIS_inv"]] call BIS_fnc_loadInventory;
                        playSound "assemble_target";
                        hint "Rearmed";
                    },
                    {},
                    [],
                    5
                ] call BIS_fnc_holdActionAdd;

                createMarkerLocal [_markerName, position _item];
                _markerName setMarkerTypeLocal "b_support";
                _markerName setMarkerColorLocal "colorNEUTRAL";

                // Update position of the marker
                [_markerName, _item] spawn {
                    params ["_markerName", "_item"];

                    while{!(isNull _item)} do {
                        _markerName setMarkerPosLocal (position _item);
                        sleep 1;
                    };
                };

                sleep _duration;
                deleteMarkerLocal _markerName;
            }] remoteExec ["BIS_fnc_call", _side, true];

            sleep _duration;
            [] remoteExec ["", _JIPId];
            deleteVehicle _item;
            detach _smoke01;
            deleteVehicle _smoke01;
            detach _smoke02;
            deleteVehicle _smoke02;
        };
    };
};