/*
	author: Kyu
	description: Get the first item index in the array validating the predicate
	returns: The item index found or -1 if not
*/

params ["_array", "_predicate"];

_value = -1;
{ // forEach

    if([_x] call _predicate) then {
        _value = _forEachIndex;
        breakOut "";
    };
} forEach _array;

// return
_value;