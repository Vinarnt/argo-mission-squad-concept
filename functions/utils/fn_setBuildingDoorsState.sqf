/*
	author: Kyu
	description: Set the doors state of a building (1: open, 0: closed)
	returns: Nothing
*/

params ["_building", "_newState"];

private ["_animationSources"];

// TODO: Could change state of a particular door by filtering with the door number (eg. _animationSources select { [_doorNumber + "_sound_source", _x] call BIS_fnc_inString };)

_animationSources = (configFile >> "CfgVehicles" >> (typeOf _building) >> "AnimationSources") call BIS_fnc_getCfgSubClasses;
_animationSources = _animationSources select { ["_sound_source", _x] call BIS_fnc_inString };

{ _building animateSource [_x, _newState] } forEach _animationSources;