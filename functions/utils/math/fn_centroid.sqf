/*
	author: Kyu
	description: Get the center of multiple points
	returns: nothing
*/

params ["_points"];
private ["_centroid", "_pointCount"];

_centroid = [0, 0];
{
    _centroid set [0, (_centroid select 0) + (_x select 0)];
    _centroid set [1, (_centroid select 1) + (_x select 1)];
} forEach _points;

_pointCount = (count _points);
_centroid set [0, (_centroid select 0) / _pointCount];
_centroid set [1, (_centroid select 1) / _pointCount];

// return
_centroid;