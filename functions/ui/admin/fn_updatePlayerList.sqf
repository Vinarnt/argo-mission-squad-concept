_listBox = uiNamespace getVariable "admin_player_list";

lbClear _listBox;
{
    _idx = _listBox lbAdd ([_x, true] call BIS_fnc_getName);
    _listBox lbSetData [_idx, getPlayerUID _x];
} forEach call BIS_fnc_listPlayers;