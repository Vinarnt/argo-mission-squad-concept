params ["_slider"];

_time = daytime;
_slider sliderSetRange [0, 24];
_slider sliderSetPosition (_time);
_slider ctrlSetTooltip format['Time %1', [_time, "HH:MM:SS"] call BIS_fnc_timeToString];
_slider ctrlCommit 0;
_slider ctrlAddEventHandler ["SliderPosChanged", {
    params ["_control", "_newValue"];

    _control ctrlSetTooltip format['Time %1', [_newValue, "HH:MM:SS"] call BIS_fnc_timeToString];
    _control ctrlCommit 0;
}];
uiNamespace setVariable['admin_time_slider', _slider];