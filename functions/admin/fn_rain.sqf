params ["_rain"];

systemChat format ["Set rain to %1", _rain];

[10, 0.5 max _rain] remoteExec ["setOvercast", 0, true];
[15, _rain] remoteExec ["setRain", 2];