/*
    author: Kyu
    description: Generate the two initial zones
    returns: The generated zones
*/

private [
        "_zoneRadius", "_initialZoneGap", "_initialZoneThreshold", "_minRadius", "_maxRadius",
        "_positionWest", "_positionEast", "_westSpawnPoint", "_eastSpawnPoint"
    ];

_zoneRadius = getNumber (missionConfigFile >> "zoneRadius");
_initialZoneGap = getNumber (missionConfigFile >> "initialZoneGap");
_initialZoneThreshold = getNumber (missionConfigFile >> "initialZoneThreshold") * 0.5;
_minRadius = _zoneRadius + _initialZoneGap - _initialZoneThreshold;
_maxRadius =  _zoneRadius + _initialZoneGap + _initialZoneThreshold;

_positionWest = [nil, ["water"]] call BIS_fnc_randomPos;
_positionEast = [_positionWest, _minRadius, _maxRadius, 0, 0, 20, 0] call BIS_fnc_findSafePos;
if(count _positionEast < 3) then { _positionEast pushBack 0; };

_westZoneMarker = createMarker ["westZoneMarker", _positionWest];
_westZoneMarker setMarkerShape "ELLIPSE";
_westZoneMarker setMarkerSize [_zoneRadius, _zoneRadius];
_westZoneMarker setMarkerColor "ColorWEST";
_westZoneMarker setMarkerAlpha 0.5;

_eastZoneMarker = createMarker ["eastZoneMarker", _positionEast];
_eastZoneMarker setMarkerShape "ELLIPSE";
_eastZoneMarker setMarkerSize [_zoneRadius, _zoneRadius];
_eastZoneMarker setMarkerColor "ColorEAST";
_eastZoneMarker setMarkerAlpha 0.5;

_westSpawnMarker = createMarker ["respawn_west_1", markerPos _westZoneMarker];
_eastSpawnMarker = createMarker ["respawn_east_1", markerPos _eastZoneMarker];


// return
[[_westZoneMarker, _westSpawnMarker, west], [_eastZoneMarker, _eastSpawnMarker, east]];
