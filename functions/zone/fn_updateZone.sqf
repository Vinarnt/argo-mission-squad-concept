/*
	author: Kyu
	description: Update the zone position
	returns: nothing
*/

params ["_zoneA", "_zoneB"];
private ["_side", "_centroid", "_players", "_positions"];

_marker = _zoneA select 0;
_side = _zoneA select 2;

_players = allUnits select { (side _x == _side) && {alive _x} && {(_x getVariable ["zoneAccountable", false]) isEqualTo true} };

// Update position only if there's at least one player alive
if(count _players > 0) then {
    if(count _players > 1) then {
        _positions = _players apply { position _x };
        _centroid = [_positions] call KF_fnc_centroid;
    } else { // If there's only one player alive, center the zone on him
        _centroid = position (_players select 0);
    };
    _marker setMarkerPos _centroid;
};

// Update the spawn positions
_zoneRadius = getNumber (missionConfigFile >> "zoneRadius");
_spawnPointOffset = getNumber (missionConfigFile >> "respawnPositionOffset");
_zoneMarker = _zoneA select 0;
_spawnMarker = _zoneA select 1;
_posZoneB = markerPos (_zoneB select 0);
_dirVec = (markerPos _zoneMarker) vectorFromTo (_posZoneB);
_offset = (-_zoneRadius) + _spawnPointOffset;
_position = markerPos _zoneMarker;
_newPosition = [0, 0];
_newPosition set [0, (_position select 0) + (_offset * (_dirVec select 0))];
_newPosition set [1, (_position select 1) + (_offset * (_dirVec select 1))];
_spawnMarker setMarkerPos _newPosition;
_spawnMarker setMarkerDir ([_newPosition, _posZoneB] call BIS_fnc_dirTo);

