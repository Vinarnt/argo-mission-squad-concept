#include "scripts\load_loadouts.sqf"
#include "initKeyboard.sqf";
#include "scripts\kill_ticker.sqf";

[] execVM "scripts\QS_icons.sqf";
[] execVM "scripts\zone_restriction.sqf";

if(!isDev) then {
    [] execVM "scripts\cutscenes\intro.sqf";
} else {
    setPlayerRespawnTime 1;
    forceRespawn player;
};

// Show tickets
call BIS_fnc_showMissionStatus;

// Disable respawn blackout
uiNamespace setVariable [ "BIS_RscRespawnControls_skipBlackOut", true ];

player addEventHandler ["InventoryOpened",{
    _this spawn {
        waitUntil {
            not isNull (findDisplay 602);
        };
        INVCLOSED = (findDisplay 602) displayAddEventHandler ["keydown", {
            _display = _this select 0;
            if ((_this select 1) == 23) then {
                (_display) displayRemoveEventHandler ["keydown", INVCLOSED];
                (_display) closeDisplay 1;
            };
        }];
    };
}];
