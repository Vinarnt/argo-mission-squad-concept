isDev = getNumber (missionConfigFile >> "isDev") == 1;

BIS_syncedTime = 0;
BIS_matchStartT = 0;
setViewDistance 1100;
setObjectViewDistance 1300;
setTimeMultiplier 10;