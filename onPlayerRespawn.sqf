params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Add nvgs
player linkItem "NVGoggles";

// Enforce side uniform
_fnc_enforceUniform = {
    params ["_uniform"];

    // Save current uniform mags
    _cargoMags = getMagazineCargo uniformContainer player;
    player forceAddUniform _uniform;

    _cargoMagsType = _cargoMags select 0;
    _cargoMagsCount = _cargoMags select 1;
    _cargoMagsLength = count _cargoMagsType;
    for [{_i = 0}, {_i < _cargoMagsLength}, {_i = _i + 1}] do {
        (uniformContainer player) addMagazineCargo [_cargoMagsType select _i, _cargoMagsCount select _i];
    };
};
if(side group player == west && {uniform player != "U_Clouds_AM"}) then {
    ["U_Clouds_AM"] call _fnc_enforceUniform;
};
if(side group player == east && {uniform player != "U_CFlames_AM"}) then {
    ["U_Flames_AM"] call _fnc_enforceUniform;
};

// Add FAK to player
(uniformContainer player) addItemCargo ["FirstAidKit", 5];

// Save loadout for rearming
[player, [missionNamespace, "BIS_inv"]] call BIS_fnc_saveInventory;